package demo.radu.shoppingcart.ui;

import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;

import demo.radu.shoppingcart.data.Cart;
import demo.radu.shoppingcart.data.Product;
import demo.radu.shoppingcart.ui.CheckoutActivity;
import demo.radu.shoppingcart.ui.CheckoutActivityFragment;
import demo.radu.shoppingcart.utils.Logger;

/**
 * Created by Radu on 10/26/2015.
 */
public class CheckoutActivityInstrumentation extends ActivityInstrumentationTestCase2<CheckoutActivity> {
    private final static String TAG = "CheckoutActivityInstrumentation";
    private CheckoutActivity mActivity;
    private Cart cart;
/*
    public CheckoutActivityInstrumentation(Class<CheckoutActivity> activityClass) {
        super(activityClass);
    }*/

    public CheckoutActivityInstrumentation() {
        super(CheckoutActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        cart = new Cart();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        mActivity = null;
        cart = null;
    }

    public void testSingleProduct() {
        Intent i = new Intent();
        cart.addToCart(new Product(0.3f, "beans"));
        i.putExtra(CheckoutActivity.EXTRA_CART, cart);
        setActivityIntent(i);
        mActivity = getActivity();
        CheckoutActivityFragment fragment = (CheckoutActivityFragment)
                mActivity.getSupportFragmentManager().getFragments().get(0);
        int count = fragment.mRecyclerView.getAdapter().getItemCount();
        String totalText = fragment.tvTotal.getText().toString();
        assertTrue(totalText.indexOf("0.3") != -1);
        Logger.getLogger().i(TAG, "Count: " + count);
        assertEquals(count, 1);
    }

    public void testTwoProductTypes() {
        Intent i = new Intent();
        cart.addToCart(new Product(0.3f, "beans"));
        cart.addToCart(new Product(1.5f, "milk"));
        i.putExtra(CheckoutActivity.EXTRA_CART, cart);
        setActivityIntent(i);
        mActivity = getActivity();
        CheckoutActivityFragment fragment = (CheckoutActivityFragment)
                mActivity.getSupportFragmentManager().getFragments().get(0);
        int count = fragment.mRecyclerView.getAdapter().getItemCount();
        Logger.getLogger().i(TAG, "Count: " + count);
        String totalText = fragment.tvTotal.getText().toString();
        assertTrue(totalText.indexOf("1.8") != -1);
        assertEquals(count, 2);
    }

    public void testThreeProductTypes() {
        Intent i = new Intent();
        cart.addToCart(new Product(0.3f, "beans"));
        cart.addToCart(new Product(1.5f, "peas"));
        Product tomato = new Product(0.5f, "milk");
        cart.addToCart(tomato);
        cart.addToCart(tomato);
        cart.addToCart(tomato);
        i.putExtra(CheckoutActivity.EXTRA_CART, cart);
        setActivityIntent(i);
        mActivity = getActivity();
        CheckoutActivityFragment fragment = (CheckoutActivityFragment)
                mActivity.getSupportFragmentManager().getFragments().get(0);
        int count = fragment.mRecyclerView.getAdapter().getItemCount();
        String totalText = fragment.tvTotal.getText().toString();
        assertTrue(totalText.indexOf("3.3") != -1);
        Logger.getLogger().i(TAG, "Count: " + count);
        assertEquals(count, 3);
    }
}
