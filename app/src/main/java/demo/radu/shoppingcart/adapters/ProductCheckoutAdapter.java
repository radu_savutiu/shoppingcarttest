package demo.radu.shoppingcart.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import co.uk.alt236.reflectivedrawableloader.ReflectiveDrawableLoader;
import demo.radu.shoppingcart.R;
import demo.radu.shoppingcart.ShoppingApp;
import demo.radu.shoppingcart.data.Cart;
import demo.radu.shoppingcart.data.Product;

/**
 * Created by Radu on 10/23/2015.
 */
public class ProductCheckoutAdapter extends RecyclerView.Adapter<ProductCheckoutAdapter.ViewHolder> {
    private final Cart cart;
    private final List<Product> productList;
    private final ReflectiveDrawableLoader reflectiveLoader;
    private final Context mContext;
    private final String currencyName;
    private final String currencySymbol;
    private final float currencyRate;

    public ProductCheckoutAdapter(Cart cart, Context context,
                                  String currencyName, String currencySymbol, float currencyRate) {
        assert (cart != null);
        this.cart = cart;
        this.reflectiveLoader = ReflectiveDrawableLoader.getInstance(context);
        this.mContext = context;
        this.productList = new ArrayList<Product>();
        this.productList.addAll(cart.getProductSet());
        this.currencyName = currencyName;
        this.currencySymbol = currencySymbol;
        this.currencyRate = currencyRate;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        @Bind(R.id.tvPrice)
        TextView tvPrice;
        @Bind(R.id.tvTitle)
        TextView tvTitle;
        @Bind(R.id.ivImage)
        ImageView ivImage;
        @Bind(R.id.tvQuantity)
        TextView tvQuantityPurchased;

        public ViewHolder(View parentView) {
            super(parentView);
            ButterKnife.bind(this, parentView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parentViewGroup, int i) {
        // create a new view
        View v = LayoutInflater.from(parentViewGroup.getContext())
                .inflate(R.layout.checkout_item_layout, parentViewGroup, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ProductCheckoutAdapter.ViewHolder viewHolder, int position) {
        assert (productList.size() > position);
        final Product p = productList.get(position);
        viewHolder.tvTitle.setText(p.getName());
        viewHolder.tvPrice.setText(String.format("%.2f %s", (p.getPrice() * currencyRate),
                currencySymbol));
        viewHolder.ivImage.setImageDrawable(mContext.getResources().getDrawable(
                reflectiveLoader.getDrawableId(p.getName(), null, R.drawable.beans)));
        viewHolder.tvQuantityPurchased.setText(String.format("%d x ", cart.getQuantityOfProduct(p)));
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }
}
