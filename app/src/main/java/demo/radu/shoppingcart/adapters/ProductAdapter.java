package demo.radu.shoppingcart.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import co.uk.alt236.reflectivedrawableloader.ReflectiveDrawableLoader;
import demo.radu.shoppingcart.R;
import demo.radu.shoppingcart.ShoppingApp;
import demo.radu.shoppingcart.data.Product;

/**
 * Created by Radu on 10/23/2015.
 */
public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    private final Product[] products;
    private final ReflectiveDrawableLoader reflectiveLoader;
    private final Context mContext;

    public ProductAdapter(Product[] productList, Context context) {
        assert(productList!=null);
        this.products = productList;
        this.reflectiveLoader = ReflectiveDrawableLoader.getInstance(context);
        this.mContext = context;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        @Bind(R.id.tvPrice) TextView tvPrice;
        @Bind(R.id.tvTitle) TextView tvTitle;
        @Bind(R.id.ivImage) ImageView ivImage;
        @Bind(R.id.addToCartBtn) Button addToCartBtn;
        @Bind(R.id.removeFromCartBtn) Button removeFromCartBtn;
        @Bind(R.id.tvQuantity) TextView tvQuantityPurchased;
        public ViewHolder(View parentView) {
            super(parentView);
            ButterKnife.bind(this, parentView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parentViewGroup, int i) {
        // create a new view
        View v = LayoutInflater.from(parentViewGroup.getContext())
                .inflate(R.layout.shopping_item_layout, parentViewGroup, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ProductAdapter.ViewHolder viewHolder, int position) {
        assert(products.length > position);
        final Product p = products[position];
        viewHolder.tvTitle.setText(p.getName());
        viewHolder.tvPrice.setText(Float.toString(p.getPrice()) + "£");
        viewHolder.ivImage.setImageDrawable(mContext.getResources().getDrawable(
                reflectiveLoader.getDrawableId(p.getName(), null, R.drawable.beans)));
        viewHolder.addToCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Snackbar.make(v, R.string.product_added, Snackbar.LENGTH_SHORT).show();
                p.buy();
                viewHolder.tvQuantityPurchased.setVisibility(View.VISIBLE);
                viewHolder.tvQuantityPurchased.setText(String.format("%d x ",
                        ShoppingApp.getAppInstance().getCart().getQuantityOfProduct(p)));
                viewHolder.removeFromCartBtn.setVisibility(View.VISIBLE);
            }
        });
        viewHolder.removeFromCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Snackbar.make(v, R.string.remove, Snackbar.LENGTH_SHORT).show();
                p.revoke();
                if (!ShoppingApp.getAppInstance().getCart().containsProduct(p)) {
                    viewHolder.removeFromCartBtn.setVisibility(View.INVISIBLE);
                    viewHolder.tvQuantityPurchased.setVisibility(View.INVISIBLE);
                }
                else {
                    viewHolder.tvQuantityPurchased.setText(String.format("%d x ",
                            ShoppingApp.getAppInstance().getCart().getQuantityOfProduct(p)));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return products.length;
    }
}
