package demo.radu.shoppingcart.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.JsonObject;

import java.util.Map;

import demo.radu.shoppingcart.BuildConfig;
import demo.radu.shoppingcart.ShoppingApp;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.QueryMap;
import retrofit.RestAdapter;


/**
 * Created by Radu on 10/25/2015.
 */
public class FixerIONetworkHelper {
    public static String TAG = "FixerIONetworkHelper";
    private static TrelloService service;

    public static TrelloService getService() {
        if (service==null) {
            RestAdapter.Builder builder = new RestAdapter.Builder().setEndpoint("https://api.fixer.io/");
            if (BuildConfig.DEBUG) {
                builder = builder.setLogLevel(RestAdapter.LogLevel.BASIC);
            }
            RestAdapter restAdapter = builder.build();
            service = restAdapter.create(TrelloService.class);
        }
        return service;
    }

    //Retrofit REST API for Fixer
    public interface TrelloService {
        @GET("/latest?base=GBP")
        void getLatestCurrencyFromBase(Callback<JsonObject> response);
    }

    /**
     * Checks whether the device currently has a network connection.
     * @return true if the device has a network connection, false otherwise.
     */
    public static boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) ShoppingApp.getAppInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }
}
