package demo.radu.shoppingcart.data;

/**
 * Created by Radu on 10/24/2015.
 */
public interface Buyable {
    public void buy();
    public void revoke();
}
