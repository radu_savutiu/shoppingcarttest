package demo.radu.shoppingcart.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import java.util.HashMap;
import java.util.Set;

import demo.radu.shoppingcart.utils.Logger;

/**
 * Created by Radu on 10/24/2015.
 */
public class Cart implements Parcelable {
    private static final String TAG = "Cart";
    public HashMap<Product, Integer> map = new HashMap<>();
    public void addToCart(Product p) {
        synchronized (map) {
            if (map.containsKey(p)) {
                int newCount = map.get(p) + 1;
                map.put(p, newCount);
            } else {
                map.put(p, 1);
            }
        }
    }

    public void removeFromCart(Product p) {
        synchronized (map) {
            if (map.containsKey(p)) {
                if (map.get(p)>1) {
                    int newCount = map.get(p) - 1;
                    map.put(p, newCount);
                }
                else {
                    map.remove(p);
                }


            } else {
                Logger.getLogger().e(TAG, "Cart does not contain product!");
            }
        }
    }

    public float checkout() {
        float total = 0;
        synchronized (map) {
            for (Product p : map.keySet()) {
                total += p.getPrice() * map.get(p);
            }
        }
        return total;
    }

    public int getQuantityOfProduct(Product p) {
        if (map.containsKey(p)) {
            return map.get(p);
        }
        else return 0;
    }

    public boolean containsProduct(Product p) {
        return map.containsKey(p);
    }

    public int getNumberOfProducts() {
        int productNumber = 0;
        synchronized (map) {
            for (Product p : map.keySet()) {
                productNumber += map.get(p);
            }
        }
        return productNumber;
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Nullable
    public Set<Product> getProductSet() {
        return map.keySet();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.map);
    }

    public Cart() {
    }

    protected Cart(Parcel in) {
        this.map = (HashMap<Product, Integer>) in.readSerializable();
    }

    public static final Parcelable.Creator<Cart> CREATOR = new Parcelable.Creator<Cart>() {
        public Cart createFromParcel(Parcel source) {
            return new Cart(source);
        }

        public Cart[] newArray(int size) {
            return new Cart[size];
        }
    };
}
