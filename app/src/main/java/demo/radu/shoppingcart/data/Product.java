package demo.radu.shoppingcart.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

import demo.radu.shoppingcart.ShoppingApp;
import demo.radu.shoppingcart.utils.Logger;

/**
 * Created by Radu on 10/23/2015.
 */
public class Product implements Buyable, Serializable {
    private static final String TAG = "Product";
    private float price;
    private String name;
    public Product(float price, String name) {
        this.price = price;
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    @Override
    public void buy() {
        Logger.getLogger().d(TAG, "adding to cart: " + name);
        ShoppingApp.getAppInstance().getCart().addToCart(this);
    }

    @Override
    public void revoke() {
        Logger.getLogger().d(TAG, "adding to cart: " + name );
        ShoppingApp.getAppInstance().getCart().removeFromCart(this);
    }

    protected Product(Parcel in) {
        this.price = in.readFloat();
        this.name = in.readString();
    }

    public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>() {
        public Product createFromParcel(Parcel source) {
            return new Product(source);
        }

        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
}
