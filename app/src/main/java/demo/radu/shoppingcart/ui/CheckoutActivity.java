package demo.radu.shoppingcart.ui;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.util.Map;

import butterknife.Bind;
import demo.radu.shoppingcart.R;
import demo.radu.shoppingcart.ShoppingApp;
import demo.radu.shoppingcart.utils.Logger;

public class CheckoutActivity extends AppCompatActivity {
    private static final String TAG = "CheckoutActivity";
    private static final String FRAGMENT_TAG = "FRAGMENT_TAG";
    public static final String EXTRA_CART = "EXTRA_CART";
    private CharSequence[] currencyNames;
    private CheckoutActivityFragment retainedFragment;
    @Bind(R.id.frame) FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.getLogger().i(TAG, "onCreate");
        setContentView(R.layout.activity_checkout);
        retainedFragment = (CheckoutActivityFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);
        if (retainedFragment==null) {
            retainedFragment = new CheckoutActivityFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.frame, retainedFragment, FRAGMENT_TAG).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_checkout, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (ShoppingApp.getAppInstance().getCurrencyRates()!=null) {
            JsonObject rates = ShoppingApp.getAppInstance().getCurrencyRates();
            if (rates.has("rates")) {
                JsonObject currencies = rates.getAsJsonObject("rates");
                currencyNames = new CharSequence[currencies.entrySet().size()];
                int i = 0;
                for (Map.Entry<String, JsonElement> entry : currencies.entrySet()) {
                    currencyNames[i++] = entry.getKey();
                }
            }
        }
        if (currencyNames==null || currencyNames.length==0) {
            MenuItem currencyItem = menu.findItem(R.id.action_currency);
            currencyItem.setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_currency) {

            new AlertDialog.Builder(this)
                    .setSingleChoiceItems(currencyNames, 0, null)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                            final String currencyName = currencyNames[selectedPosition].toString();
                            CheckoutActivityFragment newLocaleFragment = new CheckoutActivityFragment();
                            Bundle args = new Bundle();
                            args.putString(CheckoutActivityFragment.CURRENCY_NAME, currencyName);
                            JsonObject rates = ShoppingApp.getAppInstance().getCurrencyRates()
                                    .getAsJsonObject("rates");
                            JsonPrimitive currencyRateJson = rates.getAsJsonPrimitive(currencyName);
                            float currencyRate = currencyRateJson.getAsFloat();
                            args.putFloat(CheckoutActivityFragment.CURRENCY_RATE, currencyRate);
                            newLocaleFragment.setArguments(args);
                            getSupportFragmentManager().beginTransaction()
                                    .remove(retainedFragment).add(R.id.frame, newLocaleFragment, FRAGMENT_TAG).commit();
                            retainedFragment = newLocaleFragment;
                            dialog.dismiss();
                        }
                    })
                    .show();


            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
