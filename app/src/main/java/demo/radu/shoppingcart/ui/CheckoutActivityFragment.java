package demo.radu.shoppingcart.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Currency;

import butterknife.Bind;
import butterknife.ButterKnife;
import demo.radu.shoppingcart.R;
import demo.radu.shoppingcart.ShoppingApp;
import demo.radu.shoppingcart.adapters.ProductCheckoutAdapter;
import demo.radu.shoppingcart.data.Cart;
import demo.radu.shoppingcart.ui.CheckoutActivity;
import demo.radu.shoppingcart.utils.Logger;

/**
 * A placeholder fragment containing a simple view.
 */
public class CheckoutActivityFragment extends Fragment {
    public static final String CURRENCY_NAME = "CURRENCY_NAME";
    public static final String CURRENCY_RATE = "CURRENCY_RATE";
    private String currencyName = "GBP";
    private String currencySymbol = "£";
    private float currencyRate = 1.0f;
    private Cart cart;
    private static final String TAG = "CheckoutActivityFragment";
    @Bind(R.id.tvTotal) TextView tvTotal;
    @Bind(R.id.product_checkout_list) RecyclerView mRecyclerView;
    public CheckoutActivityFragment() {
        setRetainInstance(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Logger.getLogger().i(TAG, "onAttach");
        if (activity.getIntent().hasExtra(CheckoutActivity.EXTRA_CART)) {
            cart = activity.getIntent().getParcelableExtra(CheckoutActivity.EXTRA_CART);
        }
        if (cart==null) {
            cart = ShoppingApp.getAppInstance().getCart();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Logger.getLogger().i(TAG, "onCreateView");
        View parentView = inflater.inflate(R.layout.fragment_checkout, container, false);
        ButterKnife.bind(this, parentView);


        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        if (getArguments()!=null && getArguments().containsKey(CURRENCY_NAME) && getArguments().containsKey(CURRENCY_RATE)) {
            currencyName = getArguments().getString(CURRENCY_NAME);
            currencyRate = getArguments().getFloat(CURRENCY_RATE);
            currencySymbol = Currency.getInstance(currencyName).getSymbol();
        }
        // specify an adapter (see also next example)
        ProductCheckoutAdapter mAdapter = new ProductCheckoutAdapter(cart, getActivity(),
                currencyName, currencySymbol, currencyRate);
        mRecyclerView.setAdapter(mAdapter);
        tvTotal.setText(String.format("%s = %.2f %s", getString(R.string.total), cart.checkout() * currencyRate, currencySymbol));
        return parentView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Logger.getLogger().i(TAG, "onViewCreated");
        super.onViewCreated(view, savedInstanceState);
    }
}
