package demo.radu.shoppingcart.ui;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.RecyclerView;

import butterknife.Bind;
import butterknife.ButterKnife;
import demo.radu.shoppingcart.R;
import demo.radu.shoppingcart.ShoppingApp;
import demo.radu.shoppingcart.adapters.ProductAdapter;
import demo.radu.shoppingcart.data.Cart;
import demo.radu.shoppingcart.ui.CheckoutActivity;
import demo.radu.shoppingcart.utils.Logger;

/**
 * A placeholder fragment containing a simple view.
 */
public class ShopActivityFragment extends Fragment {
    private static final String TAG = "ShopActivityFragment";
    @Bind(R.id.product_list) RecyclerView mRecyclerView;
    @Bind(R.id.fabCheckout) FloatingActionButton mFabCheckout;
    public ShopActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View parentView = inflater.inflate(R.layout.fragment_shop, container, false);
        ButterKnife.bind(this, parentView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        ProductAdapter mAdapter = new ProductAdapter(ShoppingApp.getAppInstance().getAvailableProducts(), getActivity());
        mRecyclerView.setAdapter(mAdapter);

        mFabCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logger.getLogger().i(TAG, "Checkout");
                Cart cart = ShoppingApp.getAppInstance().getCart();
                if (cart.isEmpty()) {
                    Snackbar.make(v, R.string.cartEmpty, Snackbar.LENGTH_SHORT).show();
                } else {
                    Intent i = new Intent(getActivity(), CheckoutActivity.class);
                    i.putExtra(CheckoutActivity.EXTRA_CART, ShoppingApp.getAppInstance().getCart());
                    startActivity(i);
                    ShoppingApp.getAppInstance().getCart().checkout();
                }
            }
        });

        return parentView;
    }
}
