package demo.radu.shoppingcart;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import demo.radu.shoppingcart.data.Cart;
import demo.radu.shoppingcart.data.Product;
import demo.radu.shoppingcart.network.FixerIONetworkHelper;
import demo.radu.shoppingcart.utils.JSONResourceReader;
import demo.radu.shoppingcart.utils.Logger;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Radu on 10/23/2015.
 */
public class ShoppingApp extends Application {
    private static final String TAG = "App";
    private static final String CURRENCY_RATES = "CURRENCY_RATES";
    private JsonObject currencyRates;
    Product[] products;
    Cart cart;
    static ShoppingApp _instance;

    public static ShoppingApp getAppInstance() {
        return _instance;
    }

    public Product[] getAvailableProducts() {
        if (products==null) {
            JSONResourceReader reader = new JSONResourceReader(getAppInstance().getResources(), R.raw.products);
            Gson gson = new GsonBuilder().create();
            products = gson.fromJson(reader.getJsonString(), Product[].class);
        }
        return products;
    }

    public Cart getCart() {
        if (cart==null) {
            //I could also persist the Cart as a serialized JSON, if needed
            cart = new Cart();
        }
        return cart;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        _instance = this;
        if (FixerIONetworkHelper.isDeviceOnline()) {
            FixerIONetworkHelper.getService().getLatestCurrencyFromBase(new Callback<JsonObject>() {
                @Override
                public void success(JsonObject jsonObject, Response response) {
                    Logger.getLogger().i(TAG, "Network success: " + jsonObject.toString());
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ShoppingApp.this);
                    SharedPreferences.Editor e = prefs.edit();
                    e.putString(CURRENCY_RATES, jsonObject.toString());
                    e.apply();
                    currencyRates = jsonObject;
                }

                @Override
                public void failure(RetrofitError error) {
                    Logger.getLogger().e(TAG, error.getMessage() + error.getBody());
                }
            });
        }
    }

    public JsonObject getCurrencyRates() {
        if (currencyRates==null) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            if (prefs.contains(CURRENCY_RATES)) {
                String jsonString = prefs.getString(CURRENCY_RATES, "");
                currencyRates = new JsonParser().parse(jsonString).getAsJsonObject();
            }
        }
        return currencyRates;
    }
}
