package demo.radu.shoppingcart.data;

import junit.framework.TestCase;

/**
 * Created by Radu on 10/26/2015.
 */
public class ProductTest extends TestCase {
    Product p;
    public void setUp() throws Exception {
        super.setUp();
        p = new Product(0.25f, "Bread");
    }

    public void tearDown() throws Exception {

    }

    public void testGetPrice() throws Exception {
        System.out.println("testGetPrice");
        assertEquals(0.25f, p.getPrice());
    }

    public void testGetName() throws Exception {
        System.out.println("testGetName");
        assertEquals("Bread", p.getName());
    }
}