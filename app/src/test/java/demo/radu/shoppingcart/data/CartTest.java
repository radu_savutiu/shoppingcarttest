package demo.radu.shoppingcart.data;

import junit.framework.TestCase;

/**
 * Created by Radu on 10/25/2015.
 */
public class CartTest extends TestCase {
    private Cart cart;
    public void setUp() throws Exception {
        super.setUp();
        cart = new Cart();
    }

    public void tearDown() throws Exception {
        cart = null;
    }

    public void testAddProductsToCart() {
        System.out.println("testAddProductsToCart");
        assertNotNull(cart);
        Product p = new Product(0.15f, "Bread");
        cart.addToCart(p);
        assertFalse(cart.isEmpty());
        for (int i=0; i<10; i++) {
            cart.addToCart(p);
        }
        assertEquals(cart.getQuantityOfProduct(p), 11);
        assertEquals(cart.getNumberOfProducts(), 11);
    }

    public void testAddToCart() {
        System.out.println("testCheckout");
        assertNotNull(cart);
        Product bread = new Product(0.15f, "Bread");
        Product butter = new Product(0.5f, "Butter");
        Product garlic = new Product(0.12f, "Garlic");
        Product tomato = new Product(1f, "Tomato");
        Product onion = new Product(0.3f, "Onion");
        cart.addToCart(bread);
        cart.addToCart(bread);
        cart.addToCart(butter);
        cart.addToCart(butter);
        cart.addToCart(garlic);
        cart.addToCart(tomato);
        cart.addToCart(onion);
        cart.addToCart(onion);
        cart.addToCart(onion);
        assertEquals(cart.checkout(), 3.32f);
    }

    public void testRemoveFromCart() throws Exception {
        System.out.println("testRemoveProductsFromCart");
        assertNotNull(cart);
        assertTrue(cart.isEmpty());
        Product p = new Product(0.15f, "Bread");
        for (int i=0; i<10; i++) {
            cart.addToCart(p);
        }
        int i = 0;
        while (!cart.isEmpty()) {
            cart.removeFromCart(p);
            i++;
        }
        assertEquals(10, i);
    }
}